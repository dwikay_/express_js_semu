const express = require('express')
const port = 3000
var logger = require('morgan')
var cookieParser = require('cookie-parser')
var path = require('path')
var indexRouter = require('./routes/index');

var app = express()

app.use(logger('dev'));
app.use(express.json())
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', indexRouter);

app.use(function(req,res,next){
  next(createError(404))
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = app;
