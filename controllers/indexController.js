
var express = require('express')
var knex = require('../config/knex')

async function getIndex(req, res, next) {
  res.json({
    'status': 200,
    'response': 'masuk ke controller'
  })
}

async function getDataSiswa(req, res, next){

  var siswa = await knex.select('*').from('siswa')

  res.json({
    'status': 200,
    'response': siswa
  });
}

module.exports = {
  getIndex,
  getDataSiswa
};
