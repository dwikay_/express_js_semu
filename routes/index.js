var express = require('express')
var router = express.Router();
var indexController = require('../controllers/indexController')

router.get('/index/home', function(req, res,next){

  res.json({
    'status': 200,
    'response': 'stay at routes'
  })

});

router.get('/index/controller', indexController.getIndex)
router.get('/index/getDataSiswa', indexController.getDataSiswa)

module.exports = router
